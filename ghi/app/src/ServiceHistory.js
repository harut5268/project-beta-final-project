import React, { useEffect, useState } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [query, setQuery] = useState("");
    
    async function loadAppointments(){
        try {
            const response = await fetch("http://localhost:8080/api/appointments/");
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                console.error(response);
            }
        }catch (e) {
            console.error("No appointments found sorry", e);
        }
    }

    useEffect(() => {
        loadAppointments();
    }, []);

    const filteredAppointments = appointments.filter((appointment) => {
        return appointment.vin.toLowerCase().includes(query.toLowerCase());
    });

    function dateChange(date) {
        const dateTime = new Date(date);
        const dateFormatted = dateTime.toLocaleDateString();
        return dateFormatted;
    }

    function timeChange(date) {
        const dateTime = new Date(date);
        const timeFormatted = dateTime.toLocaleTimeString([],{
            hour: "2-digit",
            minute: "2-digit",
        });
        return timeFormatted;
    }

    return (
        <div className="container">
          <h1>Service History</h1>
          <div className="mb-3">
            <input
              onChange={(e) => setQuery(e.target.value)}
              type="text"
              className="form-control"
              placeholder="Enter VIN"
              value={query}
              id="vinInput"
              
            />
            <button className="btn btn-primary">Search</button>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {filteredAppointments.map((appointment) => (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.vip ? "Yes" : "No"}</td>
                  <td>{appointment.customer}</td>
                  <td>{dateChange(appointment.date_time)}</td>
                  <td>{timeChange(appointment.date_time)}</td>
                  <td>
                    {appointment.technician.first_name}{" "}
                    {appointment.technician.last_name}
                  </td>
                  <td>{appointment.reason}</td>
                  <td>{appointment.status}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    }
    
    export default ServiceHistory;
