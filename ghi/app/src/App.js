import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelsList from './VehicleModelsList';
import CreateVehicleModel from './CreateVehicleModel';
import AutomobilesList from "./AutomobilesList";
import AutomobileForm from "./AutomobileForm";
import SalespersonForm from "./SalespersonForm";
import SalespersonList from "./SalespersonList";
import CustomerList from "./CustomerList";
import CustomerForm from "./CustomerForm";
import SalesList from "./SalesList";
import RecordSaleForm from "./SaleForm";
import SaleHistory from "./SaleHistory";
import ListOfTechnician from './ListTechnicians';
import AddTechnician from './AddTechnician';
import ServiceAppointment from './CreateServiceAppointment';
import AppointmentsList from './ListServiceAppointments';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelsList />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<RecordSaleForm />} />
            <Route path="history" element={<SaleHistory />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="manufacturers" element={<ManufacturersList />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="models" element={<VehicleModelsList />} />
          <Route path="api/models" element={<CreateVehicleModel />} />
          <Route path="api/technicians" element={<ListOfTechnician />} />
          <Route path="api/technicians/create" element={<AddTechnician />} />
          <Route path="api/appointments/create" element={<ServiceAppointment />} />
          <Route path="api/appointments" element={<AppointmentsList />} />
          <Route path="api/service/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
