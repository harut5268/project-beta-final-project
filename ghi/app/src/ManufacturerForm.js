import React, { useState } from 'react';

function ManufacturerForm({ onManufacturerAdded }) {
  const [newManufacturer, setNewManufacturer] = useState({
    name: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newManufacturer),
      });

      if (response.status === 200) {
        
        setNewManufacturer({ name: '' });
      } else {
        
        console.error('Error creating manufacturer');
      }
    } catch (error) {
     
      console.error('Network error: ' + error.message);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewManufacturer({ ...newManufacturer, [name]: value });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <div className="card mt-4">
            <div className="card-header">
              <h2 className="card-title">Create a Manufacturer</h2>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label className="form-label">Name:</label>
                  <input
                    type="text"
                    name="name"
                    value={newManufacturer.name}
                    onChange={handleChange}
                    className="form-control"
                  />
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
