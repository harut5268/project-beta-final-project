import React, { useEffect, useState } from "react";

function ListOfTechnician() {
    const [technicians, setTechnicians] = useState([]);

    async function loadTechnicians() {
        try{
            const response = await fetch("http://localhost:8080/api/technicians/");
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error("No technicians found", e);
        }
    }

    useEffect(() => {
        loadTechnicians();
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map((technician) => (
                    <tr key={technician.id}>
                        <td>{technician.employee_id}</td>
                        <td>{technician.first_name}</td>
                        <td>{technician.last_name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListOfTechnician;
