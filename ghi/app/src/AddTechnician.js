import React, { useState } from 'react';

function AddTechnician() {
    const[firstName, setFirstName] = useState("");
    const[lastName, setLastName] = useState("");
    const[employeeId, setEmployeeId] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;
        

        const technicianUrl = "http://localhost:8080/api/technicians/"
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const technicianResponse = await fetch(technicianUrl, fetchOptions);
        if (technicianResponse.ok){
            const newTechnician = await technicianResponse.json();

            setFirstName("");
            setLastName("");
            setEmployeeId("");
        }
    };

    const handleFnChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };

    const handleLnChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    };


    return (
        <>
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                value={firstName}
                onChange={handleFnChange}
                placeholder="First name"
                required
                name="first_name"
                type="text"
                id="first_name"
                className="form-control"
                autoComplete="given-name"
              />
              <label htmlFor="first_name">First Name...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={lastName}
                onChange={handleLnChange}
                placeholder="Last name"
                required
                name="last_name"
                type="text"
                id="last_name"
                className="form-control"
              />
              <label htmlFor="last_name">Last Name...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={employeeId}
                onChange={handleEmployeeIdChange}
                placeholder="Employee ID"
                required
                name="employee_id"
                type="text"
                id="employee_id"
                className="form-control"
              />
              <label htmlFor="employee_id">Employee ID...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </>
      );
    }
    export default AddTechnician;
