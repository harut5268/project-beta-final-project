from django.urls import path
from .views import(
    api_list_technicians,
    api_list_appointments,
)

urlpatterns = [
    path("technicians/",api_list_technicians, name="api_list_technicians"),
    path("appointments/",api_list_appointments, name="api_list_appointments"),
    path("technicians/<int:id>/", api_list_technicians, name="delete_technicians"),
    path("appointments/<int:id>/", api_list_appointments, name="delete_appointement"),
    path("appointments/<int:id>/finish/", api_list_appointments,name="finish_appointments"),
    path("appointments/<int:id>/cancel/", api_list_appointments, name="cancel_appointments"),
]
