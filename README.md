# CarCar
​
CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.
​
Team:
​
* **Dillon** - Auto Sales
* **Harut** - Auto Services
​
## Getting Started
​
**Make sure you have Docker, Git, and Node.js 18.2 or above**
​
1. Fork this repository
​
2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>
​
3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running
​
- View the project in the browser: http://localhost:3000/
​
![Img](/images/CarCarWebsite.png)
​
## Design
​
CarCar is made up of 3 microservices which interact with one another.
​
- **Inventory**
- **Services**
- **Sales**
​
![Img](/images/CarCarDiagram.png)
​
​
## Integration - How we put the "team" in "team"
​
Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.
​
How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.
​
​
## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser
​
### Manufacturers:
​
​| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

​
​
JSON body to send data:
​
Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Ferrari"
}

```
The return value of creating, viewing, updating a single manufacturer:
```
{
			"href": "/api/manufacturers/29/",
			"id": 29,
			"name": "Ferrari"
		},
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
     {
			"href": "/api/manufacturers/29/",
			"id": 29,
			"name": "Ferrari"
		},
    }
  ]
}
```
​
### Vehicle Models:
​
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/
​
Create and update a vehicle model (SEND THIS JSON BODY)::
- In order to create a vehicle model in the database, send this JSON body that requires the model name, a picture URL, and the id of the manufacturer.
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```
​
Updating a vehicle model can take the name and/or picture URL:
- : You will need the name and/or the picture URL of a vehicle model to update it.
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
- : Getting a list of vehicle models returns a list of the detail information with the key "models".
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```
​
### Automobiles:
- POST: In order to create a automobile in the database, send this JSON body that requires the color, year, VIN, and the id of the vehicle model.
​
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/
​
​
Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/
​
Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
- PUT: You can use this methodf to update the color, year, and sold status for any automobile
{
  "color": "red",
  "year": 2012
}
```
- Getting a list of Automobile Returns the Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
# Sales Microservice
​
On the backend, the sales microservice has 4 models: AutomobileVO, Customer, SalesPerson, and SalesRecord. SalesRecord is the model that interacts with the other three models. This model gets data from the three other models.
​
The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.
​
The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.
​
| The sales app keeps track of salespeople, customers, and car sales. A poller regularly grabs info about available cars from the inventory and organizes it in the sales app. Only unsold cars show up in the sales form. When a sale is made, the inventory marks the car as sold, and the poller keeps the sales and service teams updated.

​### Salespeople

​
Action | Method | URL

    List salespeople | GET | http://localhost:8090/api/salespeople/
    Create salesperson | POST | http://localhost:8090/api/salespeople/
    Delete salesperson | DELETE | http://localhost:8090/api/salespeople/:id/


    LIST SALESPEOPLE: This endpoint will return a list of all existing salespeople in the database. No data input is required. DO NOT CHANge
        EXPECTED RESPONSE:
            {
                "salesperson": [
                    {
                        "first_name": "Dillon",
                        "last_name": "Larche",
                        "employee_id": "23231",
                        "id": 1
                    },
                    {
                        "first_name": "jerr",
                        "last_name": "fxc",
                        "employee_id": "4sff",
                        "id": 2
                    }
                ]
            }

    CREATE SALESPERSON: This endpoint allows you to create a salesperson to add to the database. data includes firname last name and id
        EXAMPLE INPUT:
            {
                "first_name": "Dill",
                "last_name": "Larche",
                "employee_id": "2"
            }

        EXPECTED RESPONSE:
            {
                "first_name": "Bill",
                "last_name": "Johnson",
                "employee_id": "12345",
                "id": 1
            }

    DELETE SALESPERSON: Sending a DELETE request to this endpoint will remove a specified salesperson from the database. There is no required data input, however you must specify the salesperson's ID in the URL when making the request.
        EXPECTED RESPONSE:
            {
                "deleted": "true"
            }



    ### Customers

    Action | Method | URL

    List customers | GET | http://localhost:8090/api/customers/
    Create customer | POST | http://localhost:8090/api/customers/
    Delete customer | DELETE | http://localhost:8090/api/customers/:id/

    LIST CUSTOMERS: This endpoint will return a list of all existing customers in the database. No data input is required.
        EXPECTED RESPONSE:
            {

  "customers": [
    {
      "id": ...,
      "name": "Martha Stewart",
      "address": "1313 Baker Street",
      "phone_number": "980720890"
    },
    {
      "id": ...,
      "name": "John Johns",
      "address": "1212 Ocean Street",
      "phone_number": "9804357878"
    }
  ]
}

## Accessing Endpoints to Send and View Data - Access through Insomnia:
​
### Customers:
​
​
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/
​
To create a Customer (SEND THIS JSON BODY):
```
{
	"name": "John Johns",
	"address": "1212 Ocean Street",
	"phone_number": 9804357878
}
```
Return Value of Creating a Customer:
```
{
	"id: "1",
	"name": "John Johns",
	"address": "1212 Ocean Street",
	"phone_number": 9804357878
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"id",
			"name": "Martha Stewart",
			"address": "1313 Baker Street",
			"phone_number": "980720890"
		},
		{
			"id",
			"name": "John Johns",
			"address": "1212 Ocean Street",
			"phone_number": "9804357878"
		}
	]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/
​
​
To create a salesperson (SEND THIS JSON BODY):
```
{
	"name": "Jane Doe",
	"employee_number": 1
}
```
Return Value of creating a salesperson:
```
{
	"id": 1,
	"name": "Liz",
	"employee_number": 1
}
```
List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"id": 1,
			"name": "Jane Doe",
			"employee_number": 1
		}
	]
}
```
### Salesrecords:
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**
​
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/salesrecords/
| Create a new sale | POST | http://localhost:8090/api/salesrecords/
| Show salesperson's salesrecords | GET | http://localhost:8090/api/salesrecords/id/
List all Salesrecords Return Value:
```
{
  "sales": [
    {
      "id": 1,
      "price": 111000,
      "vin": {
        "vin": "111"
      },
      "salesperson": {
        "id": 1,
        "name": "Liz",
        "employee_number": 1
      },
      "customer": {
        "name": "Martha Stewart",
        "address": "1313 Baker Street",
        "phone_number": "980720890"
      }
    }
  ]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"salesperson": "Liz",
	"customer": "John Johns",
	"vin": "888",
	"price": 40000
}
```
Return Value of Creating a New Sale:
```
{
  "id": 1,
  "price": 111000,
  "vin": {
    "vin": "111"
  },
  "salesperson": {
    "id": 1,
    "name": "Liz",
    "employee_number": 1
  },
  "customer": {
    "id": ...,
    "name": "Martha Stewart",
    "address": "1313 Baker Street",
    "phone_number": "9..."
  }
}
```
Show a Salesperson's Salesrecord Return Value:
```
